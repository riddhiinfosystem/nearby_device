package com.priya.myapplication;

import android.Manifest;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class ScanActivity extends AppCompatActivity {


    Button startScanningButton;
    EditText et_rssi_value;
    Button stopScanningButton;
    LinearLayout textLinearView;

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    public int etval;
    Intent intent;

    private PrefManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        intent = new Intent(this, MyService.class);
        pref = new PrefManager(ScanActivity.this);
        et_rssi_value = (EditText) findViewById(R.id.et_rssi_value);
        et_rssi_value.setText("-60");
        textLinearView = (LinearLayout) findViewById(R.id.textLinearView);
        startScanningButton = (Button) findViewById(R.id.StartScanButton);
        stopScanningButton = (Button) findViewById(R.id.StopScanButton);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String name = intent.getStringExtra(MyService.BLUETOOTH_NAME_BROADCAST);
                        int rssi = intent.getIntExtra(MyService.BLUETOOTH_RSSI_BROADCAST, 0);


                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);
                        params.setMargins(0, 10, 0, 0);

                        TextView textView = new TextView(ScanActivity.this);
                        textView.setLayoutParams(params);
                        textView.setPadding(10,0,0,0);

                        Log.e("ACTION_FOUND","Called===>"+rssi);

                        if(rssi >= pref.getRSSI())
                        {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                                textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18);
                                textView.setTextColor(getResources().getColor(R.color.Red));
                                textView.setText("Device Name: " + name + " RSSI: " + rssi );
                            }
                        }
                        else
                        {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,12);
                                textView.setTextColor(getResources().getColor(R.color.Black));
                                textView.setText("Device Name: " + name + " RSSI: " + rssi );
                            }
                        }
                        textLinearView.addView(textView);
                    }
                }, new IntentFilter(MyService.ACTION_BLUETOOTH_BROADCAST)
        );

        if(pref.getIsScan())
        {
            startScanningButton.setVisibility(View.INVISIBLE);
            stopScanningButton.setVisibility(View.VISIBLE);
        }
        else
        {
            startScanningButton.setVisibility(View.VISIBLE);
            stopScanningButton.setVisibility(View.INVISIBLE);
        }

        startScanningButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                etval = Integer.parseInt(""+et_rssi_value.getText().toString());
                if(etval < 0)
                {
                    textLinearView.removeAllViewsInLayout();
                    startScanningButton.setVisibility(View.INVISIBLE);
                    stopScanningButton.setVisibility(View.VISIBLE);
                    pref.setIsScan(true);
                    pref.setRSSI(etval);
                    OnclickCalled();
                }
                else
                {
                    Toast.makeText(ScanActivity.this,"Please Enter Minus RSSI Value",Toast.LENGTH_LONG).show();
                }
            }
        });

        stopScanningButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startScanningButton.setVisibility(View.VISIBLE);
                stopScanningButton.setVisibility(View.INVISIBLE);
                pref.setIsScan(false);
                stopService(intent);

            }
        });

        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.FOREGROUND_SERVICE}, PackageManager.PERMISSION_GRANTED);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect peripherals.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });
                builder.show();
            }
        }
    }

    public void OnclickCalled()
    {
        if(isMyServiceRunning(MyService.class))
        {
            stopService(intent);
        }
        startService(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        Log.e("onDestroy","Called===>");
        super.onDestroy();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}