package com.priya.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PrefManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "BluetoothApp";

    // All Shared Preferences Keys
    private static final String KEY_IS_Scan = "IsScan";
    private static final String KEY_RSSI = "RSSI";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean getIsScan() {
        return pref.getBoolean(KEY_IS_Scan, false);
    }
    public void setIsScan(boolean IsScan) {
        editor.putBoolean(KEY_IS_Scan, IsScan);
        editor.commit();
    }

    public void setRSSI(int RSSI) {
        editor.putInt(KEY_RSSI, RSSI);
        editor.commit();
    }
    public int getRSSI() {
        return pref.getInt(KEY_RSSI, 0);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }


}
