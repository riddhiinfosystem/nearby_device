package com.priya.myapplication;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class MyService extends Service {

    private BluetoothAdapter BTAdapter;
    public boolean isCalled = false;
    public boolean isFirstCalled = true;
    Handler handler = new Handler();
    public int tempInterval = 0;
    public int etval;
    long Delay = 5000;
    Vibrator v;
    public boolean isFirst = true;
    private PrefManager pref;

    public static final String  ACTION_BLUETOOTH_BROADCAST = MyService.class.getName() + "LocationBroadcast";
    public static  String  BLUETOOTH_NAME_BROADCAST = "BLUETOOTH_NAME_BROADCAST";
    public static  String  BLUETOOTH_RSSI_BROADCAST = "BLUETOOTH_RSSI_BROADCAST";
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implmnt");
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service started by user.", Toast.LENGTH_LONG).show();
        Log.e("version", "===" + Build.VERSION.SDK_INT);
        pref = new PrefManager(this);

        try {
            registerReceiver(receiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));

        } catch(IllegalArgumentException e) {

            e.printStackTrace();
        }



        BTAdapter = BluetoothAdapter.getDefaultAdapter();
        BTAdapter.enable();

        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        isCalled = true;
        OnclickCalled();

        return START_STICKY;
    }

    private void sendBroadcastMessage(String name,int rssi) {
        if (name != null) {
            Intent intent = new Intent(ACTION_BLUETOOTH_BROADCAST);
            intent.putExtra(BLUETOOTH_NAME_BROADCAST, name);
            intent.putExtra(BLUETOOTH_RSSI_BROADCAST, rssi);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }

    private void showNotification() {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "");
        builder.setSmallIcon(R.drawable.user_location)
                .setContentText("Do not remove notification")
                .setContentTitle("Nearby Device");
        Notification notification = builder.build();
        startForeground(123, notification);
    }

    public void OnclickCalled()
    {
        if (Build.VERSION.SDK_INT >= 26) {
            try {
                Intent notInt = new Intent(this, MainActivity.class);
                PendingIntent penInt = PendingIntent.getActivity(this, 0, notInt, 0);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "channelId");
                builder.setSmallIcon(R.drawable.user_location)
                        .setContentText("Do not remove notification")
                        .setContentTitle("Nearby Device");
                Notification notification = builder.build();
                startForeground(1, notification);
                NotificationChannel notificationChannel = new NotificationChannel("channelId", "my service", NotificationManager.IMPORTANCE_DEFAULT);
                NotificationManager notificationManager = getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(notificationChannel);
            } catch (Exception e) {
                Log.e("mylog", "===");
                e.printStackTrace();
            }
        } else {
            showNotification();
           // stopForeground(true);
            //stopSelf();
        }

        handler = new Handler();
        final Runnable task = new Runnable() {
            @Override
            public void run() {
                // Log.e("MainActivity", "Doing task");
                /*if (tempInterval > 10000) {
                    handler.removeMessages(0);
                }*/


                Log.e("OnclickCalled","Called===>");
                //if(isCalled && tempInterval>=10000)
                if(isCalled)
                {
                    Log.e("isCalled","Called===>True");

                    //tempInterval = 0;
                    if(!BTAdapter.isDiscovering())
                    {
                        Log.e("isCalled","getScanMode===>"+BTAdapter.getScanMode());

                        BTAdapter.startDiscovery();
                    }
                }
                else
                {
                    Log.e("isCalled","Called===>False");
                }

                //tempInterval = tempInterval + 5000;

                handler.postDelayed(this, Delay);
            }
        };
        handler.post(task);
    }

    @Override
    public boolean stopService(Intent name) {
        // TODO Auto-generated method stub
        Log.e("stopService","Called===>");
        Toast.makeText(this, "Service Stop by user.", Toast.LENGTH_LONG).show();
        try {

            if(receiver != null)
            {
                unregisterReceiver(receiver);
            }

        } catch(IllegalArgumentException e) {

            e.printStackTrace();
        }

        if (BTAdapter.isDiscovering()) {
            /*String myString = "";
            myString = BTAdapter.getName();
            myString = myString.replace("$", "");
            BTAdapter.setName(""+myString);*/
            BTAdapter.cancelDiscovery();
        }
        if(handler != null)
        {
            handler.removeMessages(0);
        }

        pref.setIsScan(false);

        isCalled = false;

        return super.stopService(name);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("onDestroy", "==");
        if (Build.VERSION.SDK_INT >= 26) {
            try {

                if (receiver != null) {
                    unregisterReceiver(receiver);
                }

            } catch (IllegalArgumentException e) {

                e.printStackTrace();
            }

            if (BTAdapter.isDiscovering()) {
            /*String myString = "";
            myString = BTAdapter.getName();
            myString = myString.replace("$", "");
            BTAdapter.setName(""+myString);*/
                BTAdapter.cancelDiscovery();
            }
            if (handler != null) {
                handler.removeMessages(0);
            }

            pref.setIsScan(false);

            isCalled = false;
        }
        //Toast.makeText(this, "Service destroyed by user.", Toast.LENGTH_LONG).show();
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action))
            {
                long[] pattern = {2000, 2000, 2000, 2000, 2000};
                Log.e("ACTION_FOUND","Called===>");
                int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);
                String name = intent.getStringExtra(BluetoothDevice.EXTRA_NAME);
                Log.e("ACTION_FOUND","EXTRA_NAME===>"+name);

                if(name != null)
                {
                    if(rssi >= pref.getRSSI())
                    {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            v.vibrate(VibrationEffect.createWaveform(pattern, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            //deprecated in API 26
                            v.vibrate(pattern, -1);
                        }
                    }
                    sendBroadcastMessage(name,rssi);
                }
                isCalled = true;
            }
        }
    };

}